# hpkg

If you use or enjoy this, [buy me a coffee at ko-fi](https://ko-fi.com/Q5Q2QF1PE).

hpkg is a [Rust](https://rust-lang.org) crate to manipulate Haiku's packages, repository cache files, and to programmatically scan a Haiku Ports tree

# Unit Testing

```
cargo test
```

# Example Code

```
cargo build --examples
```

# License

Copyright 2018-2020, Alexander von Gluck IV. All rights reserved.
Released under the terms of the MIT License.

# Reference

  * [On disk format](https://git.haiku-os.org/haiku/tree/headers/private/package/hpkg/HPKGDefsPrivate.h)
  * [Documentation](https://github.com/haiku/haiku/blob/master/docs/develop/packages/FileFormat.rst)
