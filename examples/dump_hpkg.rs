extern crate hpkg;

use std::{env,process};
use hpkg::package::Package;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage: hpkg_dump <package.hpkg>");
        process::exit(1);
    }

    let hpkg = match Package::load(&args[1]) {
        Ok(o) => o,
        Err(e) => {
            println!("ERROR: {:?}", e);
            assert!(false);
            return;
        },
    };
    println!("{:?}", hpkg);
}
