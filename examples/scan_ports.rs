extern crate hpkg;

use std::{env,process};
use hpkg::portstree::PortsTree;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("usage: scan_ports <ports tree base>");
        process::exit(1);
    }

    let mut ports_tree = PortsTree::new(args[1].to_string());
    let scan = ports_tree.rescan("x86_64".to_string(), None);

    if scan.is_err() {
        println!("Error scanning ports tree: {:?}", scan);
        return;
    }
    println!("Found {} ports!", scan.unwrap());
    println!("{}", ports_tree);
}
